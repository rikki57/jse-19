package ru.nlmk.study.generics.var5;

import ru.nlmk.study.generics.var5.model.Animal;
import ru.nlmk.study.generics.var5.model.Cat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<Animal> animals = Arrays.asList(new Cat("Barsik"), new Cat("Belka"));
        List<Cat> cats = Arrays.asList(new Cat("Barsik"), new Cat("Belka"));
        Collections.copy(animals, cats);
        for (Animal animal : animals){
            System.out.println(animal.getName());
        }
    }
}
