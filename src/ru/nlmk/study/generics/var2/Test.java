package ru.nlmk.study.generics.var2;

import ru.nlmk.study.generics.var2.model.Animal;
import ru.nlmk.study.generics.var2.model.Cat;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Container<Animal> container = new Container<>();
        //container.setStore(new Cat("Barsik"));
        System.out.println(container.getStore().getName());
        Container<Cat> catContainer = new Container<>();

        Cat[] cats = new Cat[]{new Cat("Daizy"), new Cat("Donald")};
        Animal[] animals = cats;
        for (Animal animal : animals){
            System.out.println(animal.getName());
        }

        List<Cat> catList = new ArrayList<>();
        List<? extends Animal> animalList = catList;

        List<Animal> animalList2 = new ArrayList<>();
        List<? super Cat> catList2 = animalList2;
    }

}
