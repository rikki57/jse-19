package ru.nlmk.study.generics.var7;

import ru.nlmk.study.generics.var6.model.Animal;

public class Container<T extends Animal> {
    private T store;

    public T getStore() {
        return store;
    }

    public void setStore(T store) {
        this.store = store;
    }

    public String sleep(String duration){

        store.sleep(Integer.valueOf(duration));
        return "OK";

    }

}
