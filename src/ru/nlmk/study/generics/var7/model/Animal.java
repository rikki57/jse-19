package ru.nlmk.study.generics.var7.model;

public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void sleep(Integer time){
        System.out.println("Sleep for " + time + " hours");
    }
}
