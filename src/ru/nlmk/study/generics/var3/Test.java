package ru.nlmk.study.generics.var3;

import ru.nlmk.study.generics.var3.model.Cat;

import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        Map<String, Cat> someMap = new HashMap();
        someMap.put("sdf", new Cat("Barsik"));
        //someMap.put(4, "sdfsdf");
        someMap.get("sdf");
    }
}
