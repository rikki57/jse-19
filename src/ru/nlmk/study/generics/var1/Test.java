package ru.nlmk.study.generics.var1;

public class Test {
    public static void main(String[] args) {
        Container<String> container = new Container();
        container.setStore("value");
        doSome(container);
        container.setStore(String.valueOf(5));
        doSome(container);
        System.out.println(((String) container.getStore()).concat(" some"));

        Container <Number> numberContainer = new Container<>();
        numberContainer.setStore(5L);

    }

    private static void doSome(Container<String> container){
        System.out.println(container.getStore());
    }
}
