package ru.nlmk.study.generics.var1;

public class Container <T> {
    private T store;

    public T getStore() {
        return store;
    }

    public void setStore(T store) {
        this.store = store;
    }
}
