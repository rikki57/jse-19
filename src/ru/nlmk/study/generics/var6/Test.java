package ru.nlmk.study.generics.var6;

import ru.nlmk.study.generics.var6.model.Animal;
import ru.nlmk.study.generics.var6.model.Cat;

public class Test {
    public static void main(String[] args) {
        Container<Animal> animalContainer = new Container<>();
        animalContainer.setStore(new Cat("Barsik"));
        animalContainer.sleep("5");
    }
}
